﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorio.Entities
{
    class Mascota
    {
        public int id { get; set; }
        public string color { get; set; }
        public string tamano { get; set; }
        public char sexo { get; set; }
        public int edad { get; set; }
        public DateTime fechaIngreso { get; set; }
        public char estado { get; set; }
        public Image foto { get; set; }
        

    }
}
