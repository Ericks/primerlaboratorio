﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorio.Entities
{
    class DuenoMascota
    {
        public int id { get; set; }
        public Dueno dueno { get; set; }
        public Mascota mascota { get; set; }
        public DateTime fechaAdopcion { get; set; }
    }
}
