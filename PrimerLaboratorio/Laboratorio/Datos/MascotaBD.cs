﻿using Laboratorio.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratorio.Datos
{
    class MascotaBD
    {
        public NpgsqlCommand cmd;
        public NpgsqlConnection conexionRetorno;
        Conexion conexion = new Conexion();

        public bool insertar(Entities.Mascota masc)
        {
            conexionRetorno = conexion.ConexionBD();
            String sql = "INSERT INTO mascota(color, tamano, sexo, edad, estado, fecha_ingreso, foto)" +
                "VALUES ('" + masc.color + "','" + masc.tamano + "','" + masc.sexo + "','" + masc.edad + "','" + masc.estado + "','"
                + masc.fechaIngreso + "','" + masc.foto + "')";

            cmd = new NpgsqlCommand(sql, conexionRetorno);

            return cmd.ExecuteNonQuery() == 1;

        }

        public bool editar(Entities.Mascota masc)
        {
            conexionRetorno = conexion.ConexionBD();
            String sql = " UPDATE mascota SET color ='" + masc.color + "', tamano ='" + masc.tamano + "', sexo ='" + masc.sexo + "', edad ='" + masc.edad + "', estado ='" + masc.estado + "', fecha_ingreso ='" + masc.fechaIngreso + "', foto ='" + masc.foto + "', nivel_ingles ='" + " WHERE id = '" + masc.id + "' ";

            cmd = new NpgsqlCommand(sql, conexionRetorno);

            return cmd.ExecuteNonQuery() == 1;
        }  
        
        public void eliminar(Entities.Mascota masc)
        {
            conexionRetorno = conexion.ConexionBD();
            cmd = new NpgsqlCommand("DELETE FROM mascota WHERE id = '" + masc.id + "'", conexionRetorno);
            cmd.ExecuteNonQuery();
            conexionRetorno.Close();
        }
    }
}
