﻿using Laboratorio.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratorio.Presentacion
{
    public partial class Principal : Form
    {
        Entities.Mascota Masc;
       

        public Principal()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {

            for (var i = 1; i <= 10; i++)
            {
                dgvMascota.Rows.Add(i, DateTime.Now);
            }

            //CalendarColumn col = new CalendarColumn();
            //this.dgvMascota.Columns.Add(col);
            //this.dgvMascota.RowCount = 5;
            //foreach (DataGridViewRow row in this.dgvMascota.Rows)
            //{
            //    row.Cells[0].Value = DateTime.Now;
            //}
        }

        private void dgvMascota_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Masc.color = dgvMascota.Rows[e.RowIndex].Cells[1].Value.ToString();
            Masc.tamano = dgvMascota.Rows[e.RowIndex].Cells[2].Value.ToString();
            Masc.sexo = (bool)dgvMascota.Rows[e.RowIndex].Cells[3].Value == true ? 'H' : 'M';
            Masc.edad = int.Parse(dgvMascota.Rows[e.RowIndex].Cells[4].Value.ToString());
            Masc.fechaIngreso = (DateTime)dgvMascota.Rows[e.RowIndex].Cells[5].Value;
            Masc.estado = (char)dgvMascota.Rows[e.RowIndex].Cells[6].Value;
            Masc.foto = (Image)dgvMascota.Rows[e.RowIndex].Cells[7].Value;


            if (new MascotaBO().registrar(Masc))
            {
                MessageBox.Show("Mascota registrada con exito");
                this.Close();
            }

        }
    }
}

