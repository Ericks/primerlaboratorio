﻿namespace Laboratorio.Presentacion
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvMascota = new System.Windows.Forms.DataGridView();
            this.txtID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbxColor = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cbxTamano = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.chkSexo = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.txtEdad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtpFecha = new Laboratorio.Presentacion.CalendarColumn();
            this.txtEstado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imgFoto = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMascota)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMascota
            // 
            this.dgvMascota.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMascota.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtID,
            this.cbxColor,
            this.cbxTamano,
            this.chkSexo,
            this.txtEdad,
            this.dtpFecha,
            this.txtEstado,
            this.imgFoto});
            this.dgvMascota.Location = new System.Drawing.Point(12, 12);
            this.dgvMascota.Name = "dgvMascota";
            this.dgvMascota.Size = new System.Drawing.Size(776, 426);
            this.dgvMascota.TabIndex = 0;
            this.dgvMascota.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMascota_CellValueChanged);
            // 
            // txtID
            // 
            this.txtID.DataPropertyName = "ID";
            this.txtID.HeaderText = "ID";
            this.txtID.Name = "txtID";
            this.txtID.Visible = false;
            // 
            // cbxColor
            // 
            this.cbxColor.DataPropertyName = "Color";
            this.cbxColor.HeaderText = "Color";
            this.cbxColor.Items.AddRange(new object[] {
            "Blanco",
            "Negro",
            "Gris",
            "Café"});
            this.cbxColor.Name = "cbxColor";
            // 
            // cbxTamano
            // 
            this.cbxTamano.DataPropertyName = "Tamano";
            this.cbxTamano.HeaderText = "Tamaño";
            this.cbxTamano.Items.AddRange(new object[] {
            "Grande",
            "Mediano",
            "Pequeño"});
            this.cbxTamano.Name = "cbxTamano";
            // 
            // chkSexo
            // 
            this.chkSexo.DataPropertyName = "Sexo";
            this.chkSexo.HeaderText = "Sexo";
            this.chkSexo.Name = "chkSexo";
            // 
            // txtEdad
            // 
            this.txtEdad.DataPropertyName = "Edad";
            this.txtEdad.HeaderText = "Edad";
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.Width = 80;
            // 
            // dtpFecha
            // 
            this.dtpFecha.DataPropertyName = "FechaIngreso";
            this.dtpFecha.HeaderText = "Fecha de ingreso";
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Width = 150;
            // 
            // txtEstado
            // 
            this.txtEstado.DataPropertyName = "Estado";
            this.txtEstado.HeaderText = "Estado";
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Width = 120;
            // 
            // imgFoto
            // 
            this.imgFoto.DataPropertyName = "Foto";
            this.imgFoto.HeaderText = "Foto";
            this.imgFoto.Name = "imgFoto";
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dgvMascota);
            this.Name = "Principal";
            this.Text = "Mascota";
            ((System.ComponentModel.ISupportInitialize)(this.dgvMascota)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMascota;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtID;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbxColor;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbxTamano;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkSexo;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtEdad;
        private CalendarColumn dtpFecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtEstado;
        private System.Windows.Forms.DataGridViewImageColumn imgFoto;
    }
}