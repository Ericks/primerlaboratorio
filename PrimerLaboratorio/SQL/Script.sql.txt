CREATE TABLE mascota
(
	id serial primary key,
	color text not null,
	tamano text not null,
	sexo char not null,
	edad int not null,
	estado char not null,
	fecha_ingreso date not null,
	foto bytea not null
)

select * from mascota 



CREATE TABLE dueno
(
	id serial primary key,
	cedula text not null,
	nombre text not null
)

select * from dueno


CREATE TABLE duenomascota
(
	id serial primary key,
	id_mascota int not null,
	id_dueno int not null,
	fecha_adopcion date not null
	
	constraint fk_adop_mas foreign key(id_mascota) references mascota(id),
	constraint fk_adop_mas foreign key(id_dueno) references dueno(id),
	
)

select * from duenomascota




